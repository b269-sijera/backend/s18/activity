function number(num){
	let add = num + 15;
	console.log("Displayed sum of 5 and 15");
	console.log(add)

	let minus = 20 - num;
	console.log("Displayed difference of 20 and 5");
	console.log(minus);
}
number(5);

function number2(num2){
	let product = num2 * 10;
	console.log("The product of 50 and 10: ");
	console.log(product);

	let quotient = num2 / 10;
	console.log("The quotient of 50 and 10: ");
	console.log(quotient);
}
number2(50);

function piValue(pi) {
	let area = pi * 15 ** 2;
	console.log("The result of getting the are of a circle with 15 radius: ");
	console.log(area);
}
piValue(3.14);

function grade(numOfGrades){
	let average = (20 + 40 + 60 + 80) / numOfGrades;
	console.log("The average of 20, 40, 60, and 80: ");
	console.log(average);
}
grade(4);

function test(score) {
	let passingScore = score / 50 * 100;
	console.log("Is 38/50 a passing score?");
	let examScore = passingScore >= 75;
	console.log(examScore);
}
test(38);
